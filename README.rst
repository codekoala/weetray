Weechat Message Notifier
========================

This is a combination of a weechat plugin (written in Python) and a desktop
application (written in Go) to receive message notifications from weechat.

Weechat Plugin (weedis)
-----------------------

The Python plugin can be configured to notify or ignore messages in specific
channels. When a message comes in from a channel that is not ignored, the
message will be sent to a Redis server. By default, it will try to connect to a
Redis server on the same system that is running weechat. If you need to
customize this, you can use::

    /set plugins.var.python.weedis.host custom.redishost.com

If you want to receive notifications from a specific channel, use the following
command in that channel::

    /weedis notify

Alternatively, you can specify names of the channels from which you wish to
receive notifications::

    /weedis notify channel1 channel2 channel3

Similarly, you can use the ``ignore`` command to stop receiving notifications
from a channel::

    /weedis ignore
    /weedis ignore channel1 channel2 channel3

If you find that you're receiving notifications for your own messages, it's
likely because of some prefix on your nick. You can specify the characters you
wish to strip from your nick using the ``strip`` command::

    /weedis strip @+

Desktop Application (weetray)
-----------------------------

Currently, the application is only really tested for use on Linux (it's
compiled using gtk/glib for a tray icon). With some effort, it could probably
work on other platforms as well.

The desktop application's sole purpose is to present notifications using
libnotify whenever the Python plugin receives a message from a channel that is
not ignored. The plugin will toss the message into Redis, and the desktop
application will receive the Redis notification.

By default, ``weetray`` will connect to the Redis server on the local system.
If you wish to use a remote Redis server, you may launch ``weetray`` using the
``-host`` flag::

    ./weetray -host custom.redishost.com

Installing Weedis and Weetray
=============================

You can get the latest version of the weedis weechat plugin with the following
command::

    curl https://drone.io/bitbucket.org/codekoala/weetray/files/weedis.py > ~/.weechat/python/autoload/weedis.py

You can get the latest version of the weetray desktop application with the
following command::

    curl -O https://drone.io/bitbucket.org/codekoala/weetray/files/weetray
    chmod +x weetray

Disclaimer
==========

This is one of my first Go applications. There are absolutely bugs that need to
be fixed. Please report any bugs you find so I can try to reproduce them and
get them fixed.