#!/usr/bin/env python2

"""
WeeChat Redis Notifier
======================

This plugin will publish messages received to a Redis pubsub channel, and it
will track unread message counts in a particular Redis database.

Configurable options:

* ``host``: the hostname or IP of the redis host. Default: ``127.0.0.1``
* ``port``: the port that redis is listening on. Default: ``6379``
* ``db``: the database used to store unread message counts. Default: ``15``

In order to modify these settings, simply use::

    /set plugins.var.python.weedis.[option] [value]

"""

from functools import partial
import json
import time

import redis

try:
    from weechat import (
        buffer_get_string as _get,
        config_get_plugin as cfg,
        WEECHAT_RC_OK as OK,
        WEECHAT_RC_ERROR as ERR
    )
    import weechat
    import_ok = True
except ImportError:
    import_ok = False


SCRIPT_NAME = 'weedis'
SCRIPT_AUTHOR = 'codekoala'
SCRIPT_VERSION = '0.1.2'
SCRIPT_LICENSE = 'GPL3'
SCRIPT_DESC = 'Publish received messages to redis'
NICK_STRIP = ' @+'


if __name__ == '__main__' and import_ok:
    # register the plugin with weechat
    weechat.register(SCRIPT_NAME, SCRIPT_AUTHOR, SCRIPT_VERSION,
                     SCRIPT_LICENSE, SCRIPT_DESC, 'close', '')

    # handle weechat events
    weechat.hook_print('', '', '', 1, 'notify', '')
    weechat.hook_signal('input_text_changed', 'typing', '')
    weechat.hook_signal('buffer_switch', 'buffer_switch', '')
    weechat.hook_signal('window_switch', 'window_switch', '')

    # handle custom commands
    weechat.hook_command('weedis', 'configure weedis', 'notify|ignore|strip', 'My arguments yo', 'herf', 'weedis_cmd', '')


# tags that we're interested in
MSG_TAGS = {'irc_privmsg', 'notify_message'}

MSG_CHAN = 'weechat-msg'
UNREAD_CHAN = 'weechat-unread'
UNREAD_PRE = '%s-' % (UNREAD_CHAN,)
UNREAD_KEY = '%s%%s' % (UNREAD_PRE,)
NOTIFY_KEY = 'weechat-notify'
IGNORE_KEY = 'weechat-ignore'
STRIP_KEY = 'weechat-strip'
LAST = dict.fromkeys(['payload', 'time', 'channel'])

# Redis connection stuff
HOST = cfg('host') or '127.0.0.1'
PORT = int(cfg('port') or 6379)
DB = int(cfg('db') or 15)
CLI = None


def log(msg):
    """Log a message"""

    weechat.prnt('', "weedis: %s" % (msg,))


def cli():
    """
    Return a handle to Redis.
    """

    global CLI

    if CLI is None:
        log("Attempting to connect to redis at %s:%s:%s" % (HOST, PORT, DB))
        CLI = redis.StrictRedis(
            host=HOST,
            port=PORT,
            db=DB
        )

    return CLI


def buffer_name(buf):
    """
    Helper to get the short buffer name or fallback to the normal buffer name.

    :param buf:
        A WeeChat buffer.

    :returns:
        A string representing the name of `buf`.

    """

    name = _get(buf, 'short_name') or _get(buf, 'name')

    return name.strip()


def window_name(win):
    """
    Helper to get the name of a WeeChat window.

    :param win:
        A WeeChat window.

    :returns:
        A string representing the name of `win`.

    """

    return 'win_%s' % weechat.window_get_integer(win, 'number')


def weedis_cmd(data, buf, args):
    chats = [buffer_name(buf)]
    args = args.split()
    if not args:
        log('Please use /weedis strip, /weedis notify, or /weedis ignore')
        return OK

    cmd = args[0]
    if len(args) > 1:
        chats = args[1:]

    if cmd == 'strip':
        strip = ''.join(set(chats + [' ']))
        log("Now stripping the following characters from nicks: {}".format(strip))
        cli().set(STRIP_KEY, strip)
    elif cmd == 'notify':
        log("Now notifying for messages from: {}".format(', '.join(chats)))
        cli().sadd(NOTIFY_KEY, *chats)
        cli().srem(IGNORE_KEY, *chats)
    elif cmd == 'ignore':
        log("Now ignoring messages from: {}".format(', '.join(chats)))
        cli().sadd(IGNORE_KEY, *chats)
        cli().srem(NOTIFY_KEY, *chats)
    else:
        log('Invalid command: {}'.format(args))

    return OK


def notify(data, buf, date, tags, displayed, hilight, prefix, msg):
    """
    Main event handler. Most WeeChat events will pass through this function.

    :param data:
    :param buf:
    :param date:
    :param tags:
    :param displayed:
    :param hilight:
    :param prefix:
    :param msg:

    """

    # bail early if we aren't interested in this event
    tags = {t.strip() for t in tags.split(',') if t}
    if not tags & MSG_TAGS:
        return OK

    get = partial(_get, buf)

    nick_strip = cli().get(STRIP_KEY) or NICK_STRIP

    # get some basic information about the message
    chat = buffer_name(buf)
    server = get('localvar_server').strip()
    my_nick = weechat.info_get('irc_nick', server).strip(nick_strip)
    channel = get('localvar_channel').strip()
    prefix = prefix.strip(nick_strip)

    # whether or not a message shall be published
    _send = True
    event = None

    if get('localvar_type') == 'private':
        event = 'pm'
        _send = chat == prefix
    elif hilight == "1":
        event = 'mention'
    elif tags & MSG_TAGS:
        event = 'message'
    else:
        _send = False

    notify = bool(cli().sismember(NOTIFY_KEY, chat)) or event in ('pm', 'mention')

    # force no notification if something is specifically ignored
    ignore = bool(cli().sismember(IGNORE_KEY, chat))
    ignore |= bool(cli().sismember(IGNORE_KEY, prefix))
    if ignore:
        notify = False

    # don't notify about messages that the user said
    if _send and prefix != my_nick:
        if notify:
            incr_count_for(chat)

        send({
            'event': event,
            'user': prefix,
            'chat': chat,
            'msg': msg,
            'server': server,
            'channel': channel,
            'my_nick': my_nick,
            'notify': notify,
        }, MSG_CHAN)

    return OK


def typing(data, signal, buf):
    """Reset any unread count when typing into a buffer"""

    name = buffer_name(buf)
    reset_count_for(name)

    return OK


def buffer_switch(data, signal, buf):
    """Reset any unread count when switching to a buffer"""

    name = buffer_name(buf)
    reset_count_for(name)

    return OK


def window_switch(data, signal, win):
    """Reset any unread count when switching to a window"""

    name = window_name(win)
    reset_count_for(name)

    return OK


def incr_count_for(name):
    """Increment the number of unread messages for `name`"""

    cli().incr(UNREAD_KEY % (name,))
    send_unread()


def reset_count_for(name):
    """Reset any unread count for `name`"""

    cli().delete(UNREAD_KEY % (name,))
    send_unread()


def send_unread():
    """Publish the latest unread counts"""

    send({
        key.replace(UNREAD_PRE, ''): int(cli().get(key))
        for key in cli().keys(UNREAD_KEY % ('*',))
    })


def send(payload, channel=UNREAD_CHAN):
    """
    Only send the message if it's changed or been more than 15 seconds since
    the last message.

    """

    now = time.time()

    lt = LAST['time']
    lp = LAST['payload']
    lc = LAST['channel']

    if lt < now - 15 or payload != lp or channel != lc:
        cli().publish(channel, json.dumps(payload))

    LAST.update(dict(
        time=now,
        payload=payload,
        channel=channel
    ))


def close():
    """Clean up our redis connection"""

    try:
        cli().close()
        rc = OK
    except Exception as ex:
        log('Failed to close channel: %s' % (ex,))
        rc = ERR

    return rc
