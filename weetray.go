package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/garyburd/redigo/redis"
	dbus "github.com/guelfey/go.dbus"
	"github.com/mattn/go-gtk/glib"
	"github.com/mattn/go-gtk/gtk"
	"html"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"
)

const (
	APP_TITLE   = "WeeTray"
	APP_VERSION = "0.2.0"
	NOTIFY_DEST = "org.freedesktop.Notifications"
	NOTIFY_PATH = "/org/freedesktop/Notifications"
	PLAYER      = "/usr/bin/paplay"
	SOUND       = "/usr/share/sounds/pop.wav"

	UNREAD_CHAN = "weechat-unread"
	MSG_CHAN    = "weechat-msg"

	ICON_NONE     = gtk.STOCK_APPLY
	ICON_UNREAD_1 = gtk.STOCK_YES
	ICON_UNREAD_2 = gtk.STOCK_NO
)

var (
	APP_TITLE_VERSION = fmt.Sprintf("%s %s", APP_TITLE, APP_VERSION)
	UnreadPrefix      = UNREAD_CHAN + "-"
	info              *log.Logger
	_err              *log.Logger
	unreadCounts      = make(map[string]int)
	debugMode         bool

	redisHost = flag.String("host", "127.0.0.1", "Redis host IP")
	redisPort = flag.Int("port", 6379, "Redis port number")
	redisDb   = flag.Int("db", 15, "Redis database number")
	server    string

	statusIcon *gtk.StatusIcon

	newPm    = make(chan *PM, 20)
	quitChan = make(chan os.Signal, 1)
)

/**
 * A message received in weechat.
 **/
type PM struct {
	Event   string
	User    string
	Chat    string
	Msg     string
	Server  string
	Channel string
	MyNick  string
	Notify  bool
}

// Extract information from the message published to Redis and do something
// about it
func HandleUnread(msg *redis.Message) {
	info.Println("In HandleUnread")

	// discard any existing message count information
	unreadCounts = make(map[string]int)

	if err := json.Unmarshal(msg.Data, &unreadCounts); err != nil {
		_err.Println("Failed to unmarshall unread count info")
		return
	}

	UpdateStatus()
}

func UpdateStatus() {
	info.Println("Updating status: %v", unreadCounts)

	hasUnread := false
	icon := ""
	tip := "You have unread messages:"

	for name, count := range unreadCounts {
		tip = fmt.Sprintf("%s\n%s: %v", tip, html.EscapeString(name), count)
		hasUnread = true
	}

	if hasUnread {
		icon = ICON_UNREAD_1
	} else {
		icon = ICON_NONE
		tip = "No unread messages"
	}

	statusIcon.SetFromStock(icon)
	statusIcon.SetTooltipMarkup(tip)
}

func HandleMsg(msg *redis.Message) {
	info.Println("In HandleMsg")

	res := &PM{}

	err := json.Unmarshal(msg.Data, &res)
	if err != nil {
		_err.Println("Failed to unmarshal PM info")
		return
	}

	if res.Notify {
		newPm <- res
	}
}

func NotifyOfPM() {
	info.Println("In NotifyOfPM")

	// dbus for notifications
	dbus_conn, err := dbus.SessionBus()
	if err != nil {
		panic(err)
	}
	info.Println("Got DBUS session handle")
	notifier := dbus_conn.Object(NOTIFY_DEST, NOTIFY_PATH)
	info.Println("Got DBUS notifier handle")

	for {
		info.Println("Waiting on PM...")
		pm := <-newPm
		info.Println("Received PM")
		go notifier.Call(NOTIFY_DEST+".Notify", 0, "",
			uint32(0), "", pm.User, pm.Msg, []string{},
			map[string]dbus.Variant{}, int32(10000))

		go exec.Command(PLAYER, SOUND).Run()
	}
}

// Handle all incoming messages published to redis
func HandleIncoming(psc *redis.PubSubConn) {
	info.Println("In HandleIncoming")
	for {
		info.Println("Awaiting message from redis...")
		switch v := psc.Receive().(type) {
		case redis.Message:
			if v.Channel == UNREAD_CHAN {
				go HandleUnread(&v)
			} else if v.Channel == MSG_CHAN {
				go HandleMsg(&v)
			}
		}
	}
}

func GetCurrentStatus() {
	info.Println("Creating redis connection...")
	redisConn, err := redis.Dial("tcp", server)
	if err != nil {
		panic(err)
	}
	defer redisConn.Close()

	info.Println("Fetching keys...")
	keys, _ := redis.Values(redisConn.Do("KEYS", UnreadPrefix+"*"))

	for _, key := range keys {
		chat := fmt.Sprintf("%s", key)
		chat = strings.Replace(chat, UnreadPrefix, "", 1)

		v, _ := redisConn.Do("GET", key)
		val := fmt.Sprintf("%s", v)
		intVal, _ := strconv.Atoi(val)
		unreadCounts[chat] = intVal
	}

	info.Println("Updating initial status...")
	UpdateStatus()

	Subscribe(redisConn)
}

func Subscribe(conn redis.Conn) {
	info.Println("Creating PubSub connection...")
	psc := redis.PubSubConn{conn}

	// subscriptions
	info.Println("Subscribing to channels...")
	psc.Subscribe(UNREAD_CHAN)
	psc.Subscribe(MSG_CHAN)

	HandleIncoming(&psc)
}

func blink() {
	for {
		var icon string

		switch statusIcon.GetStock() {
		case ICON_NONE, ICON_UNREAD_2:
			icon = ICON_UNREAD_1
		case ICON_UNREAD_1:
			icon = ICON_UNREAD_2
		}

		if len(unreadCounts) == 0 {
			icon = ICON_NONE
		}

		statusIcon.SetFromStock(icon)
		time.Sleep(1 * time.Second)
	}
}

func main() {
	logDest := ioutil.Discard

	// Command line arguments
	flag.BoolVar(&debugMode, "d", false, "Show log messages")
	flag.BoolVar(&debugMode, "debug", false, "Show log messages")
	flag.Parse()

	if debugMode {
		logDest = os.Stdout
	}

	info = log.New(logDest, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	_err = log.New(logDest, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
	server = fmt.Sprintf("%s:%v", *redisHost, *redisPort)

	signal.Notify(quitChan, os.Interrupt)

	// Initialize GTK for a tray icon
	gtk.Init(&os.Args)
	glib.SetApplicationName(APP_TITLE)
	defer gtk.MainQuit()

	// Setup tray icon
	statusIcon = gtk.NewStatusIconFromStock(gtk.STOCK_REFRESH)
	statusIcon.SetTitle(APP_TITLE_VERSION)
	statusIcon.SetTooltipMarkup(APP_TITLE_VERSION)

	// Setup exit menu item
	exitMI := gtk.NewMenuItemWithLabel("Quit")
	exitMI.Connect("activate", func() {
		quitChan <- syscall.SIGINT
	})

	// Make a menu
	menu := gtk.NewMenu()
	menu.Append(exitMI)
	menu.ShowAll()

	// Display menu when right clicking tray icon
	statusIcon.Connect("popup-menu", func(cbx *glib.CallbackContext) {
		menu.Popup(nil, nil, gtk.StatusIconPositionMenu, statusIcon, uint(cbx.Args(0)), uint32(cbx.Args(1)))
	})

	// Handler for when the icon is clicked normally
	statusIcon.Connect("activate", func(cbx *glib.CallbackContext) {
		info.Println("Clearing unread messages")

		// discard any existing message count information
		unreadCounts = make(map[string]int)
		UpdateStatus()
	})

	// Throw all of the useful stuff into goroutines
	go GetCurrentStatus()
	go NotifyOfPM()
	go gtk.Main()
	go blink()

	// Wait for the signal to quit
	for {
		select {
		case <-quitChan:
			return
		}
	}
}

// vim:ft=go noet sw=8 ts=8 ai:
